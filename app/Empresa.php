<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Utils\Utils;
use NFePHP\NFe\ToolsNFe;

class Empresa extends Model
{
    
    public function salvarEmpresa($empresaRequest)    
    {
 
		$retorno = ['sucesso' => true, 'mensagem' => 'Empresa salva com sucesso!'];

    	try
    	{
	    	$validacao = $this->validaEmpresa($empresaRequest);	    	

			if(!$validacao['sucesso'])
			{			
				return $validacao;
			}

			//Verifica se o CNPJj já está cadastrado			
			$existeEmpresa = Empresa::where('cnpj', Utils::limpaCNPJ($empresaRequest['empresa']['cnpj']))->first();			
			if(isset($existeEmpresa))
				return ['sucesso' => false, 'mensagem' => 'Empresa já está cadastrada!'];;

			
		    $arquivoCertificado = $this->gravarCertificado($empresaRequest);
					
			if(!$arquivoCertificado){
				return ['sucesso' => false, 'mensagem' => 'Arquivo do certificado digital inválido!'];
			}

			$config = $this->geraConfiguracoesEmpresa($empresaRequest,$arquivoCertificado);
			
			//Testa para ver se vai conseguir iniciar o NFePHP
			$nfe = new ToolsNFe($config);		

			if(isset($nfe))	{//Se conseguiu passar, aí sim salva informações no database

				$empresa = new Empresa();
				$empresa->razaosocial = $empresaRequest['empresa']['razaosocial'];
				$empresa->nomefantasia = $empresaRequest['empresa']['nomefantasia'];
				$empresa->cnpj = Utils::limpaCNPJ($empresaRequest['empresa']['cnpj']);
				$empresa->ie = $empresaRequest['empresa']['ie'];
				$empresa->status = 1;
				$empresa->save();

				if(isset($empresa->id))
				{
					$certificado = new certificado();	
					$certificado->empresa_id = $empresa->id;
					$certificado->senha =   Utils::criptografaSenha($empresaRequest['certificado']['senha']);
					$certificado->endarquivo = Utils::getPastaCerts() . '\\' .$arquivoCertificado;
					$certificado->nomearquivo = $arquivoCertificado;
					$dataVencimento = Date('Y-m-d',$nfe->getTimestampCert());
					$certificado->datavalidade = $dataVencimento;
					$certificado->status = 1;//1: Ativo, 2: Inativo
					$certificado->save();					
				}

			}

		}catch(Exception $e){
			echo 'Ocorreram Erros: ' . $e-getMessage();
		}

		return $retorno;
    }


	/**
	*Valida campos do objeto de cadastro de empresa	
	*@empresa Parâmetro com dados de empresa.
	*Retorno: Sucesso ou falha de acordo com validações
	*/
    private function validaEmpresa($empresa)
    {

    	$retorno = ['sucesso' => true, 'mensagem' => ''];

    	if(empty($empresa))
    	{
    		$retorno['sucesso'] = false;
			$retorno['mensagem'] = "JSON Vazio!";	
    	}
    		

    	//Verifica existência dos campos
    	if( !isset( $empresa['empresa'] ) ||
    		!isset( $empresa['empresa']['cnpj'] ) ||
    		!isset( $empresa['empresa']['ie'] ) ||
    		!isset( $empresa['empresa']['razaosocial'] ) ||
    		!isset( $empresa['empresa']['nomefantasia'] ) ||
    		!isset( $empresa['certificado'] ) ||
    		!isset( $empresa['certificado']['senha'] ) ||
    		!isset( $empresa['certificado']['arquivo'] )
    	  )
		{
			$retorno['sucesso'] = false;
			$retorno['mensagem'] = "Formato Json inválido!";
	    }

	    //Verifica campos obrigatórios
	    if( 
    		empty( $empresa['empresa']['cnpj'] ) ||
    		empty( $empresa['empresa']['ie'] ) ||
    		empty( $empresa['empresa']['razaosocial'] ) ||
    		empty( $empresa['empresa']['nomefantasia'] ) ||
    		empty( $empresa['certificado']['senha'] ) ||
    		empty( $empresa['certificado']['arquivo'] )
    	  )
		{
			$retorno['sucesso'] =	 false;
			$retorno['mensagem'] = "Campos inválidos ou vazios!";
			
	    }
	
	    if(strlen(Utils::limpaCNPJ($empresa['empresa']['cnpj'])) != 14)
	    {
			$retorno['sucesso'] = false;
			$retorno['mensagem'] = "Campo CNPJ ínválido!";
	    }

	    return $retorno;
    }

	/**
	*Grava certificado digital em disco.
	*@empresa Parâmetro com dados de empresa.
	*Retorno: Sucesso ou falha se conseguiu salvar em disco
	*O nome do arquivo segue a regra: certificado_cnpj_segundosatuais.pfx
	*/
	private function gravarCertificado($empresa)
	{

		$retorno = false;		

		$cnpj = Utils::limpaCNPJ($empresa['empresa']['cnpj']);
		$tempo = time();
		$certificado = base64_decode($empresa['certificado']['arquivo']);
		$nomeArquivo = "certificado_" . $cnpj . '_' . $tempo . ".pfx";		
		$arquivo = file_put_contents( Utils::getPastaCerts() . '\\' . $nomeArquivo, $certificado);		
		if($arquivo)
			$retorno = $nomeArquivo;

		return $retorno;

	}

	/**
	*Monta configuração para instanciar o objeto ToolsNFe.
	*@empresa Parâmetro com dados de empresa.
	*@arquivoCertNome: Nome do certificado gravado em disco.	
	*Retorno Arquivo JSON.
	*/
	private function geraConfiguracoesEmpresa($empresa,$arquivoCertNome)
	{

		$data = Date('y-m-d h::m:s');
		$config = 
		'
			{
			    "atualizacao":"'. $data .'",
			    "tpAmb":1,
			    "pathXmlUrlFileNFe":"nfe_ws3_mod55.xml",
			    "pathXmlUrlFileCTe":"cte_ws1.xml",
			    "pathXmlUrlFileMDFe":"mdfe_ws1.xml",
			    "pathXmlUrlFileCLe":"cle_ws1.xml",
			    "pathXmlUrlFileNFSe":"",
			    "pathNFeFiles":"'. str_replace('\\','\/', Utils::getPastaNFe() ).'\/' . '",
			    "pathCTeFiles":"'. str_replace('\\','\/', Utils::getPastaCTe() ) .'\/'. '",
			    "pathMDFeFiles":"'. str_replace('\\','\/', Utils::getPastaMDFe() ). '\/' . '",
			    "pathCLeFiles":"'. str_replace('\\','\/', Utils::getPastaCLe() ). '\/' . '",
			    "pathNFSeFiles":"' . str_replace('\\','\/', Utils::getPastaNFSe() ). '\/' . '",
			    "pathCertsFiles":"'. str_replace('\\','\/',Utils::getPastaCerts() ).'\/' . '",
			    "siteUrl":"http://localhost:8080/sdfeconsultasefaz/",
			    "schemesNFe":"PL_008h2",
			    "schemesCTe":"PL_CTE_104",
			    "schemesMDFe":"MDFe_100",
			    "schemesCLe":"CLe_100",
			    "schemesNFSe":"",
			    "razaosocial":"' . $empresa['empresa']['razaosocial'] . '",
			    "siglaUF":"",
			    "cnpj":"' . Utils::limpaCNPJ($empresa['empresa']['cnpj']) . '",
			    "tokenIBPT":"AAAAAAA",
			    "tokenNFCe":"GPB0JBWLUR6HWFTVEAS6RJ69GPCROFPBBB8G",
			    "tokenNFCeId":"000002",
			    "certPfxName":"'. $arquivoCertNome .'",
			    "certPassword":"' . $empresa['certificado']['senha'] .  '",
			    "certPhrase":"tajomstvo",
			    "aDocFormat":{
			        "format":"P",
			        "paper":"A4",
			        "southpaw":"1",
			        "pathLogoFile":"",
			        "logoPosition":"L",
			        "font":"Times",
			        "printer":"hpteste"},
			    "aMailConf":{
			        "mailAuth":"1",
			        "mailFrom":"roberto@myapp.local",
			        "mailSmtp":"smtp.myapp.local",
			        "mailUser":"roberto@myapp.local",
			        "mailPass":"heslo$",
			        "mailProtocol":"ssl",
			        "mailPort":"587",
			        "mailFromMail":null,
			        "mailFromName":null,
			        "mailReplayToMail":null,
			        "mailReplayToName":null,
			        "mailImapHost":null,
			        "mailImapPort":null,
			        "mailImapSecurity":null,
			        "mailImapNocerts":null,
			        "mailImapBox":null},
			    "aProxyConf":{
			        "proxyIp":"",
			        "proxyPort":"",
			        "proxyUser":"",
			        "proxyPass":""}
		    }
		';

		return $config;

	}

 }
