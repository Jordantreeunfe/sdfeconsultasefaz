<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Empresa;
use Utils\Utils;

class EmpresaController extends Controller
{
    
    public function store(Request $request)
    {    	

		$requestJSON = $request->json()->all();        
		$modelEmpresa = new Empresa();		        
		return $modelEmpresa->salvarEmpresa($requestJSON);
  		
    }

    public function index()
    {
    	//$time = time();
    	//$date = Date('Y-m-d', $time);
    	$senha = 'birubiru';

    	return Utils::criptografaSenha($senha);
    }
}
