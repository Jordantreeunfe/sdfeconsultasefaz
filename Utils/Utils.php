<?php

namespace Utils;

class Utils
{
	public static function limpaCNPJ($cnpj)
	{
		$arrNoisesCNPJ = ['.','-','/'];
		$cnpj = str_replace($arrNoisesCNPJ,'',$cnpj);

		return $cnpj;
	}

	public static function getPastaNFe()
	{
		return base_path().'\\nfe';
	}

	public static function getPastaCTe()
	{
		return base_path().'\\cte';
	}

	public static function getPastaMDFe()
	{
		return base_path().'\\mdfe';
	}

	public static function getPastaCLe()
	{
		return base_path().'\\cle';
	}

	public static function getPastaNFSe()
	{
		return base_path().'\\nfse';
	}

	public static function getPastaCerts()
	{
		if(!file_exists(base_path().'\\certs')){
			mkdir(base_path().'\\certs', 0777);
		}

		return base_path().'\\certs';
	}

	public static function criptografaSenha($senha)
	{

		$senha = md5($senha . 'sdfeconsulta');

		return $senha;
	}

	
}